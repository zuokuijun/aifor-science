# AIForScience

Tips:
如何提交代码以及拉取代码（前提是已经安装了git代码管理工具）

**从远端仓库拉取代码**

首先从仓库右上角复制仓库的SSH地址
<p align="center">
    <img src="./images/git1.png"/>
</p>

然后使用`git clone  [仓库地址] `命令从远端克隆代码
<p align="center">
    <img src="./images/git2.png"/>
</p>

注意！！！在提交代码之前一定要用`git pull origin  master`从远端拉取代码,不然会造成与本地代码冲突

编辑完成后,使用如下命令提交代码到远程仓库：

git add .        # 将修改内容添加到本地仓库   

git  commit -m "具体提交内容"  # 填写本次提交内容

git push origin  master  # 将修改的内容提交到远程仓库


___

本仓库主要记录流体力学与机器学习相关知识

___

<p align="center">
    <img src="./images/cover.jpg" />
</p>



## 流体力学

* [常用术语总结](./docs/professional.md)
* [流体的连续性、黏性与可压缩性](./docs/aerodynamics1.md)
* [随体导数或者物质导数的定义](./docs/material_derivative.md)
* [连续性方程](./docs/continuous.md)
* [动量方程](./docs/momentum.md)
* [能量方程](./docs/energy.md)
* [流体力学控制方程总结](./docs/control.md)

## 书籍

* [推荐书籍](./docs/book.md)

## 风雷程序相关

* [边界条件](./docs/phenglei_boundary.md)
* [冻结变量设置](./docs/forzen-variables.md)
