# 冻结变量以结构求解器为例

* Step 1 找到 Simulation.cpp 于此文件中修改

* Step 2  于.h代码中添加

  ```c++
  void WriteData();
  //创建文件
  //void CreatFile(int index, RDouble ma, RDouble aoa, RDouble re);
  void WriteCenterDataForML(int index);
  void ReadData();
  void ReadCenterDataForML(int index);
  ```

  于.cpp代码中添加

  ```c++
  void Simulation::ReadData()
  {
      WriteLogFile("Reading flowfield data ...");
      using namespace PHMPI;
      int nLocalZones = GetNumberofLocalZones();
      int nTotalZones = GetNumberofGlobalZones();
      int count = 0;
  
      for (int iZone = 0; iZone < nTotalZones; ++iZone)
      {
          if (GetGrid(iZone, 0) == 0)
          {
              continue;
          }
          else
          {
              ReadCenterDataForML(iZone);
              count += 1;
              if (count == nLocalZones)
              {
                  break;
              }
          }
      }
  
  }
  
  void Simulation::ReadCenterDataForML(int index)
  {
      using namespace PHSPACE;
      StructGrid* grid = StructGridCast(GetGrid(index, 0));
      RDouble3D& viscousTurbulent = *reinterpret_cast<RDouble3D*> (grid->GetDataPtr("vist"));
  
      int ist, ied, jst, jed, kst, ked;
      grid->GetCellIterationIndex(ist, ied, jst, jed, kst, ked);
      RDouble4D &qTurbulent       = *reinterpret_cast<RDouble4D *> (grid->GetDataPtr("q_turb" ));
      int wordwidth = 20;
      string filename = "./results/viscousTurbulent.dat";
      fstream file0;
      file0.open(filename, ios_base::in);
      using namespace IDX;
      
      for (int k = kst; k <= ked; ++k)
      {
          for (int j = jst; j <= jed; ++j)
          {
              for (int i = ist; i <= ied; ++i)
              {
                  file0 >> setw(wordwidth) >> i >> 
                      setw(wordwidth) >> j >> 
                      setw(wordwidth) >> k >> 
                      setw(wordwidth) >>  viscousTurbulent(i, j, k) ;
              }
          }
      }
      file0.close();
      file0.clear();  
  }
  
  void Simulation::WriteData()
  {
      WriteLogFile("Writing flowfield data ...");
      using namespace PHMPI;
      int nLocalZones = GetNumberofLocalZones();
      int nTotalZones = GetNumberofGlobalZones();
      int count = 0;
  
      for (int iZone = 0; iZone < nTotalZones; ++iZone) 
      {
          if (GetGrid(iZone, 0) == 0) 
          {
              continue;
          }
          else 
          {
              WriteCenterDataForML(iZone);
              count += 1;
              if (count == nLocalZones) 
              {
                  break;
              }
          }
      }
  
  }
  
  // zly 添加代码,输出结构网格的湍流涡粘
  void Simulation::WriteCenterDataForML(int index)
  {
      using namespace PHSPACE;
      StructGrid* grid = StructGridCast(GetGrid(index, 0));
      RDouble3D& viscousTurbulent = *reinterpret_cast<RDouble3D*> (grid->GetDataPtr("vist"));
  
  int ist, ied, jst, jed, kst, ked;
  grid->GetCellIterationIndex(ist, ied, jst, jed, kst, ked);
  RDouble4D &qTurbulent       = *reinterpret_cast<RDouble4D *> (grid->GetDataPtr("q_turb" ));
  int wordwidth = 20;
  string filename = "./results/viscousTurbulent.dat";
  fstream file0;
  file0.open(filename, ios_base::app);
  using namespace IDX;
  for (int k = kst; k <= ked; ++k)
  {
      for (int j = jst; j <= jed; ++j)
      {
          for (int i = ist; i <= ied; ++i)
          {
              file0 << setw(wordwidth) << i <<
                  setw(wordwidth) << j <<
                  setw(wordwidth) << k <<
                  setw(wordwidth) << viscousTurbulent(i, j, k) << endl;
          }
      }
  }
  file0.close();
  }
  ```
  分别在SolveSimulation();函数前后运行程序

  Step 3  若冻结涡粘请在TurbSolverStruct.cpp的TurbSolverStr::ComputeViscousCoeff(Grid *grid)中注释Viscosity(grid);
  
  











