%已知激波前Ma 激波角 求 激波后数值 
%假设激波前无量纲化 v=1 rho=1 p=1/GAMA/MA^2

gama=1.4
Ma1=2.25                                %激波前马赫数
beta=33.2*pi/180                        %激波角
t=2*cot(beta)*(Ma1*Ma1*sin(beta)*sin(beta)-1)/(Ma1*Ma1*(gama+cos(2*beta))+2)
theta= atan(t)                          %偏折角
V2n=sin(beta)*cot(beta)/cot(beta-theta) %激波后垂直于激波速度分量
V1t=cos(beta)                           %激波前后平行于激波速度分量
V2x=V2n*sin(beta)+V1t*cos(beta)         %激波后平行于x轴速度
V2y=V2n*cos(beta)-V1t*sin(beta)         %激波后平行于y轴速度
rho2=(gama+1)*Ma1*Ma1*sin(beta)*sin(beta)/(2+(gama-1)*Ma1*Ma1*sin(beta)*sin(beta))
p2= 1+2*gama*(Ma1*Ma1*sin(beta)*sin(beta)-1)/(gama+1)
p2=p2/gama/Ma1/Ma1
%xr=8.626
%xs=8.549
%delta= (xr-xs)*25.4
