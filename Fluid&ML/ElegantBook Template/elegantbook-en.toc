\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Elegant\LaTeX {} Templates}{1}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Online Usage}{1}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Portable Version}{1}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Update Templates}{1}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Other Release Versions}{1}{section.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}ElegantBook Settings}{2}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Languages}{2}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Device Mode Option}{2}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Color Themes}{2}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Cover}{3}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Customized Cover}{3}{subsection.2.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Cover Image}{4}{subsection.2.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.3}Logo}{4}{subsection.2.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.4}Stylized Cover}{4}{subsection.2.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Chapter Title Display Styles}{4}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Introduction of Math Environments}{4}{section.2.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.1}Theorem Class Environments}{5}{subsection.2.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.2}Counter for Theorem Environments}{5}{subsection.2.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.3}Other Customized Environments}{6}{subsection.2.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}List Environments}{6}{section.2.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.8}Fonts}{6}{section.2.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8.1}Symbol Fonts}{7}{subsection.2.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.9}Bibliography}{7}{section.2.9}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.10}Preface}{8}{section.2.10}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.11}Content Option and Depth}{8}{section.2.11}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.12}Introduction Environment}{8}{section.2.12}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{Chapter\nobreakspace {}2\nobreakspace {}\nobreakspace {}Exercise}{9}{section*.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.13}Margin Notes}{9}{section.2.13}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}ElegantBook Writing Sample}{11}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Writing Sample}{11}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Second section}{12}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{Chapter\nobreakspace {}3\nobreakspace {}\nobreakspace {}Exercise}{13}{section*.7}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}FAQ}{14}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Version History}{15}{chapter.5}%
\defcounter {refsection}{0}\relax 
\renewcommand {\cftchappresnum }{Appendix }\renewcommand {\cftchapaftersnum }{}\setlength {\cftchapnumwidth }{\widthof {\textbf {Appendix\nobreakspace {}999}}} 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Mathematical Tools}{17}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Summation Operator and Description Statistics}{17}{section.A.1}%
