## phenglei 边界条件

* 边界条件函数所在位置 void NSSolverStruct::Boundary(Grid* gridIn)（NSSolverStruct.cpp）

* 边界条件变量定义所在namespace PHSPACE中（BC.h中）

* 添加入射激波所需边界条件

  * step 1 BC.h中添加

  * <p align="center">
        <img src="../images/reflect-step1.png" />
    </p>

  * step 2 NSSolverStruct.h中

  * <p align="center">
        <img src="../images/reflect-step2.png" />
    </p>
  
    
* step 3 void NSSolverStruct::Boundary(Grid *gridIn) 中
  
* 
  
* <p align="center">
        <img src="../images/reflect-step3.png" />
    </p>
  
* step 4 创建函数（内容复制inflowbc3d中）
  
* <p align="center">
        <img src="../images/reflect-step4.png" />
    </p>
  
* step 5 修改参数 将constanbc3d函数中变量修改为波后参数
  
* <p align="center">
        <img src="../images/reflect-step5.png" />
    </p>
  
* 激波前后关系式可见 code 文件夹下 obiqueshock.m
  
* step6 在turb求解器下调用此边界条件void TurbSolverStr::Boundary(Grid *gridIn)中
  
* 
  
* <p align="center">
        <img src="../images/reflect-step6.png" />
    </p>
  
* 
  
  在风雷计算中需要手动改边界条件 为991 即可开始计算
  
  int nBoundaryConditons = 4;
    string bcName = "BCWall";
    {
      int bcType = 2;
    }
    string bcName = "BCFarfield";
    {
      int bcType = 991;
    }
    string bcName = "BCInflow";
    {
      int bcType = 5;
    }
    string bcName = "BCOutflow";
    {
      int bcType = 6;
    }
  
  <p align="center">
        <img src="../images/reflect-step7.png" />
    </p>
    
  
    
  
  
