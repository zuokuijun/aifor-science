\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}空气动力学基础}{1}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}基础概念总结}{1}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}随体导数的定义}{2}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}连续性方程}{4}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}流体微元}{4}{subsection.1.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.2}有限控制体}{6}{subsection.1.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}动量方程推导}{7}{section.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}能量方程的推导}{11}{section.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.6}雷诺平均方法}{11}{section.1.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.6.1}连续性方程的雷诺平均}{12}{subsection.1.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.6.2}动量方程的雷诺平均}{12}{subsection.1.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.7}湍流模型的分类}{13}{section.1.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.8}零方程模型}{14}{section.1.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.8.1}Prandtl混合长度假设}{14}{subsection.1.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.9}一方程模型}{15}{section.1.9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.9.1}Kolmogorov-Prandtl模型}{15}{subsection.1.9.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.9.2}Spalart-Allmaras(SA)湍流模型}{16}{subsection.1.9.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.10}二方程模型}{16}{section.1.10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.10.1}$k-\varepsilon $湍流模型}{16}{subsection.1.10.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.10.2}$k-\omega $湍流模型}{19}{subsection.1.10.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.10.3}$SST$湍流模型}{19}{subsection.1.10.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}机器学习基础}{20}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}模型降阶方法之经典POD方法}{20}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}模型降阶方法之快照POD方法}{20}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}模型降阶方法之SVD POD方法}{21}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}在线使用模板}{28}{subsection.2.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}本地免安装使用}{28}{subsection.2.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}发行版安装与更新}{28}{subsection.2.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}其他发行版本}{29}{subsection.2.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}关于提交}{29}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}强化学习}{30}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}语言模式}{30}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}设备选项}{30}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}颜色主题}{30}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}封面}{31}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}封面个性化}{31}{subsection.3.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}封面图}{31}{subsection.3.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}徽标}{32}{subsection.3.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.4}自定义封面}{32}{subsection.3.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}章标标题}{32}{section.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}数学环境简介}{32}{section.3.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.1}定理类环境的使用}{33}{subsection.3.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.2}修改计数器}{33}{subsection.3.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6.3}其他环境的使用}{33}{subsection.3.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.7}列表环境}{34}{section.3.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.8}参考文献}{34}{section.3.8}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.9}添加序章}{34}{section.3.9}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.10}目录选项与深度}{34}{section.3.10}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.11}章节摘要}{35}{section.3.11}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.12}章后习题}{35}{section.3.12}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{第 3{} 章\nobreakspace {}练习}{35}{section*.14}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.13}旁注}{36}{section.3.13}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}迁移学习}{37}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}数学字体选项}{37}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}使用 newtx 系列字体}{37}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}连字符}{37}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}宏包冲突}{37}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}中文字体选项}{38}{section.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}方正字体选项}{38}{subsection.4.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}其他中文字体}{38}{subsection.4.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}ElegantBook 写作示例}{40}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Lebesgue 积分}{40}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}积分的定义}{40}{subsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{第 5{} 章\nobreakspace {}练习}{42}{section*.17}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}常见问题集}{43}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}版本更新历史}{44}{chapter.7}%
\defcounter {refsection}{0}\relax 
\renewcommand {\cftchappresnum }{附录 }\renewcommand {\cftchapaftersnum }{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}基本数学工具}{47}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}求和算子与描述统计量}{47}{section.A.1}%
